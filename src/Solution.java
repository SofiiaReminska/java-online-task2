import java.util.Random;
import java.util.Scanner;

public class Solution {
    public static void main(String[] args) {
        System.out.println("people at the party: ");
        Scanner sc = new Scanner(System.in);
        int numberOfGuests = sc.nextInt();
        int attempts = 1000000;
        int previousGuest = 0;
        int countRumorHeard = 0;
        int success = 0;
        for (int i = 0; i < attempts; i++) {
            boolean[] guests = new boolean[numberOfGuests];
            guests[0] = true;
            int currentGuest = 1 + (int) (Math.random() * (numberOfGuests - 1));
            while (!guests[currentGuest]) {
                guests[currentGuest] = true;
                previousGuest = currentGuest;
                while (currentGuest == previousGuest) {
                    currentGuest = 1 + (int) (Math.random() * (numberOfGuests - 1));
                }
                countRumorHeard++;
            }
            int count = 0;
            for (boolean guest : guests) {
                if (guest) {
                    count++;
                }
            }
            if (count == guests.length) {
                success++;
            }

        }
        System.out.println("probability: " + (double) success / attempts);
        System.out.println("average number of guests: " + countRumorHeard / attempts);
        sc.close();
    }
}